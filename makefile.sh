#!/usr/bin/env bash
echo 'Hi there :)'
echo  '─────────▄──────────────▄'
echo '────────▌▒█───────────▄▀▒▌'
echo '────────▌▒▒▀▄───────▄▀▒▒▒▐'
echo '───────▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐'
echo '─────▄▄▀▒▒▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐'
echo '───▄▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀██▀▒▌'
echo '──▐▒▒▒▄▄▄▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄▒▒▌'
echo '──▌▒▒▐▄█▀▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐'
echo '─▐▒▒▒▒▒▒▒▒▒▒▒▌██▀▒▒▒▒▒▒▒▒▀▄▌'
echo '─▌▒▀▄██▄▒▒▒▒▒▒▒▒▒▒▒░░░░▒▒▒▒▌'
echo '─▌▀▐▄█▄█▌▄▒▀▒▒▒▒▒▒░░░░░░▒▒▒▐'
echo '▐▒▀▐▀▐▀▒▒▄▄▒▄▒▒▒▒▒░░░░░░▒▒▒▒▌'
echo '▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒░░░░░░▒▒▒▐'
echo '─▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒▒▒░░░░▒▒▒▒▌'
echo '─▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐'
echo '──▀▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▒▒▒▒▌'
echo ' ────▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀'
echo ' ───▐▀▒▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄'

source venv/bin/activate
pip install -r requirements.txt
cd django
python manage.py migrate
python manage.py loaddata fixtures/users.json
python manage.py loaddata fixtures/api.json
python manage.py runserver
python -mwebbrowser http://localhost:8000