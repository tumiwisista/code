# Skychallenge

## SETUP && INSTALATION
````
$ mkdir <name>

$ git clone https://tumiwisista@bitbucket.org/tumiwisista/code.git

$ chmod 777 ./makefile.sh && ./makefile.sh  #<---IF THIS ISN'T WORKING FOR YOU 

or

$ chmod +x makefile.sh && ./makefile.sh  # <--- AND THIS :(

# I HAVE BAD NEWS, YOU HAVE TO TYPE FOLLOWING INSTRUCTIONS:

$ source virtualenv/bin/activate

$ pip install -r requirements.txt

$ cd django

$ python manage.py migrate

$ python manage.py loaddata fixtures/user.json

$ python manage.py loaddata fixtures/exam.json

$ python manage.py test   // Tests not exixts yet

$ python manage.py runserver
````
# You have 5 accounts to play with.
### Logins: Skygateuser1, Skygateuser2 etc. 
### Password to all of them: wszechnica

##Chapter I

### The goal is to create a REST API for a web application that solves the problem of preparing and evaluating exam sheets.
Required functionalities:

* addition, edition, deletion/archivization of exam sheets by the owner;

* defining the appropriate permissions (access rights) for the owner (sheets creator) and a normal user for individual resources or actions;

* possibility for the exam owner to assess each task, assign points for it and a final grade for the entire exam;

* exam sheets filtering e.g. by users/owners, scores, etc.

# EXAM API

##/
swagger

##/api/

GET - Root

## api/exams/

### GET - LIST
###`curl -X GET "http://localhost:8000/api/exams/" -H  "accept: application/json" -H  "X-CSRFToken: p8hyhD9aL1WRSLVs4nVOWwXFWnQltInnRcq4xWDBWU7EikA8oJE1ek42k420To4g"`
### FILTERS `http://localhost:8000/api/exams/?owner=1&grades=1`
### POST - ADD EXAM/TASK EVAL POINT/GRADE


## GET PUT PATCH DELETE {ID}

## api/answers/

## GET LIST
### POST `curl -X POST "http://localhost:8000/api/answer/" -H  "accept: application/json" -H  "Content-Type: application/json" -H  "X-CSRFToken: p8hyhD9aL1WRSLVs4nVOWwXFWnQltInnRcq4xWDBWU7EikA8oJE1ek42k420To4g" -d "{  \"text\": \"My answer is 7892.\",  \"task\": 3,  \"exam\": 1}"`
#### Example output:
`response 201 {
  "id": 14,
  "student": 1,
  "text": "My answer is 7892.",
  "task": 3,
  "exam": 1
}`
## GET PUT PATCH DELETE {ID}





##Chapter II

### 1. Skyphrases - How many skyphrases are valid? 
Answer: 383.

````
$ python ChapterII/Task1/test_skyphrases.py

$ python ChapterII/Task1/skyphrases.py
````
### 2. I write my programs in json - What is the sum of all numbers in the document?
Answer: 111754.
````
$ python ChapterII/Task2/test_counter.py

$ python ChapterII/Task2/counter.py
````


#TODO:

### * TESTS.
