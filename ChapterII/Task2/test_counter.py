#!/usr/bin/python
import requests
import re
# link to raw json
url = 'https://pastebin.com/raw/azc6e9fD'
# fetching url
res = requests.get(url)


def test_url():
    res.status_code = requests.codes.ok
    # if url is valid, output should be 200
    assert res.status_code == 200, 'Should be 200'


def test_len_first_five():
    first_word = res.text[:5]
    assert first_word == '{"e":', 'The first word should be {"e":'


def test_len_last_five():
    last_word = res.text[-5:]
    assert last_word == 'et"}}', 'The last word should be et"}}'


def test_cleaning_data():
    """otput of data: {"e":[[{"e":86,"c":23,"a":{"a":[120,169,"green","red",
    "orange"],"b":"red"},"g":"yellow","b":["yellow"],"d":"red","f":-19}"""
    data = res.text[:121]
    # cleaning data (letters etc.) after cleaning we got list with numbers
    cleaning = re.findall(r'(-*\d+)', data)
    assert cleaning == ['86', '23', '120', '169', '-19'], 'Cleaned data'


def test_sum_cleaned_data():
    # cleaned_data from upper test
    cleaned_data = ['86', '23', '120', '169', '-19']
    # sum of cleaned_data list, btw is 379 *)
    cleaned_data_sum = sum([int(n) for n in cleaned_data])
    assert cleaned_data_sum == 379, 'Cleaned data sum should be 379'


if __name__ == "__main__":
    test_url()
    test_len_first_five()
    test_len_last_five()
    test_cleaning_data()
    test_sum_cleaned_data()
    print("5 tests passed")

