#!/usr/bin/python
"""

Skyphrases

In skygate, a new system policy has been put in place that requires all
accounts to use a specialpassphrase - skyphrase - instead of simply a
password. A skyphrase consists of a series of words (lowercase letters)
separated by spaces. To ensure security, a valid skyphrase must contain no
duplicate words.

For example:
- aa bb cc dd ee is valid.
- a bb cc dd aa is not valid - the word aa appears more than once.
- bb cc dd aaa is valid - aa and aaa count as different words.

The system's full skyphrase list is available as your puzzle input under
the link: https://pastebin.com/3u4QViN8

How many skyphrases are valid?

Answer: 383.
"""
import requests


def skyphrases_checker():
    url = 'https://pastebin.com/raw/3u4QViN8'
    res = requests.get(url)
    valid = 0
    not_valid = 0
    fetched_data = res.text
    fetched = fetched_data.splitlines()

    for line in fetched:
        b = line.split()
        c = set(b)
        if len(b) == len(c):
            valid += 1
        else:
            not_valid += 1
    return f'How many skyphrases are valid? \n ' \
           f'Answer: {valid} valid phrases. So we have {not_valid} not valid phrases.'


if __name__ == "__main__":
    skyphrases_checker()
    print(skyphrases_checker())
