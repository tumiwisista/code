#!/usr/bin/python
import requests

# link to raw's skyphares text
url = 'https://pastebin.com/raw/3u4QViN8'
# fetching url
res = requests.get(url)
data = res.text


def test_url():
    res.status_code = requests.codes.ok
    # if url is valid, output should be 200
    assert res.status_code == 200, 'Should be 200'


def test_first_word():
    first_word = res.text[:6]
    assert first_word == 'sayndz', 'The first word should be sayndz'


def test_last_word():
    last_word = res.text[-4:]
    assert last_word == 'gcne', 'The last word should be gcne'


def test_line_count():
    # all lines in the list
    lines = data.splitlines()
    print(lines)
    # checking count of list
    list_count = len(lines)
    assert list_count == 512, 'Should be 512 phrases'


def test_checker():
    valid = 0
    not_valid = 0
    # first 10 lines; we have 8 valid and 2 unvalid phrases here;
    fetched_data = res.text[:485]
    # list
    fetched = fetched_data.splitlines()
    for line in fetched:

        b = line.split()
        c = set(b)
        if len(b) == len(c):
            valid += 1
        else:
            not_valid += 1
    assert valid == 8, 'Valid should be 8.'
    assert not_valid == 2, 'We have 2 unvalid here'


if __name__ == "__main__":
    test_url()
    test_first_word()
    test_last_word()
    test_line_count()
    test_checker()

    print('5 test passed.')

