from rest_framework import serializers
from .models import (
    Exam, Task, Answer, Point, Grade
                     )
from django.contrib.auth.models import User


class GradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grade
        fields = (
            'id',
            'grade',
            'note',
            'student'
        )


class PointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Point
        fields = (
            'id',
            'point'
        )


class AnswerSerializer(serializers.ModelSerializer):
    student = serializers.HiddenField(
        default=serializers.CurrentUserDefault())
    points = PointSerializer(many=True,
                             required=False)

    class Meta:
        model = Answer
        fields = ('id',
                  'student',
                  'text',
                  'points'
                  )


class StudentAnswerSerializer(serializers.ModelSerializer):
    student = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Answer
        fields = '__all__'


class TaskSerializer(serializers.ModelSerializer):
    task_answer = AnswerSerializer(many=True,
                                   required=False)

    class Meta:
        model = Task
        fields = (
            'id',
            'question',
            'task_answer'
        )


class ExamSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(read_only=True)
    questions = TaskSerializer(many=True,
                               required=False)

    grades = GradeSerializer(many=True,
                             required=False)
    eval = PointSerializer(many=True,
                           required=False)

    class Meta:
        model = Exam
        fields = (
            'id',
            'name',
            'owner',
            'is_archived',
            'created',
            'grades',
            'questions',
            'eval',
        )


class UserSerializer(serializers.ModelSerializer):
    exams = ExamSerializer(many=True)

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'exams'
        )
