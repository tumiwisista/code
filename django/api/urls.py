# api/urls.py
from django.urls import include, path
from rest_framework.routers import DefaultRouter
from api.views import ExamViewSet, UserViewSet, AnswersViewSet

app_name = 'exam'
router = DefaultRouter()
router.register('exams', ExamViewSet)
router.register('users', UserViewSet)
router.register('answer', AnswersViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
