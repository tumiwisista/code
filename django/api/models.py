# api/models.py
from django.db import models
from django.conf import settings


class Exam(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE,
                              related_name='exams')
    name = models.CharField(max_length=255)
    is_archived = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.name


class Task(models.Model):
    exam = models.ForeignKey(Exam,
                             on_delete=models.CASCADE,
                             related_name='questions')
    question = models.CharField(blank=True, max_length=200)

    class Meta:
        unique_together = ('exam', 'question')

    def __str__(self):
        return self.question


class Answer(models.Model):
    student = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                related_name='exam_answers')
    task = models.ForeignKey(Task,
                             on_delete=models.CASCADE,
                             related_name='task_answer')
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    text = models.TextField(max_length=300)

    class Meta:
        unique_together = ('exam', 'task', 'student')


class Point(models.Model):
    point = models.SmallIntegerField(default=0)
    answer = models.ForeignKey(Answer,
                               on_delete=models.CASCADE,
                               related_name='points')
    task = models.ForeignKey(Task,
                             on_delete=models.CASCADE)
    exam = models.ForeignKey(Exam,
                             on_delete=models.CASCADE,
                             related_name='eval')

    class Meta:
        unique_together = ('exam', 'task', 'answer')


class Grade(models.Model):
    GRADE = (
        ('a', 'A'),
        ('b', 'B'),
        ('c', 'C'),
        ('d', 'D'),
        ('e', 'E'),
        ('f', 'F'),
    )
    note = models.CharField(max_length=50, blank=True)
    grade = models.CharField(max_length=1, choices=GRADE)
    student = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                related_name='grade')
    exam = models.ForeignKey(Exam,
                             on_delete=models.CASCADE,
                             related_name='grades')

    class Meta:
        unique_together = ('exam', 'student', 'grade')

    def __str__(self):
        return self.grade
