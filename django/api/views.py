# api/views.py
from .models import Exam, Answer
from .serializers import (
    ExamSerializer, UserSerializer, StudentAnswerSerializer
)
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from .permission import IsOwnerOrReadOnly, IsStudentOrReadOnly
from django.contrib.auth.models import User


class ExamViewSet(viewsets.ModelViewSet):
    """
     retrieve:
     Returns exam by id
     list:
     Returns all exist exams with flag [is_archive=False]
     create:
     Creating exam for authenticated users
     update:
     Exam updating is only for owner
     partial_update:
     Also you can change exam status to archived [is_archive=True]
     delete:
     Only owner can delete own exam
     """
    serializer_class = ExamSerializer
    queryset = Exam.objects.all().filter(is_archived=False)
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    filterset_fields = ('owner', 'name', 'grades')

    def perform_create(self, serializer):
        owner = self.request.user
        serializer.save(owner=owner)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class AnswersViewSet(viewsets.ModelViewSet):
    """
         retrieve:
         Returns answer by id
         list:
         Returns all exist answers
         create:
         Creating answer for authenticated users/ to create you have choose /
         exam id and task id to answer
         update:
         Edition only for student
         partial_update:
         generic partial
         delete:
         Only owner can delete own exam
         """

    serializer_class = StudentAnswerSerializer
    queryset = Answer.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly, IsStudentOrReadOnly]
    filterset_fields = ('student', 'exam', 'task')

    def perform_create(self, serializer):
        student = self.request.user
        serializer.save(student=student)
