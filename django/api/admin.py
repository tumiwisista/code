# api/admin.py
from django.contrib import admin
from .models import Exam, Task, Answer, Point, Grade


models = (Exam, Task, Answer, Point, Grade)

admin.site.register(models)
