"""config/urls.py

config URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Exam sheet API')

urlpatterns = [
    path('accounts/', admin.site.urls),
    path('', schema_view),
    path('api/', include('api.urls', namespace='exam')),
]
